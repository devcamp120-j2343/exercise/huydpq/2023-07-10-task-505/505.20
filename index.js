// import thư viện express
const express = require('express')

// khỏi tạp app express
const app = express();

// khỏi tạo cổng chạy
const port = 8000;

app.get("/", (req,res) =>{
    let today = new Date() ;
    res.json({
        message: `
        Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}
        `
    })

})
app.get("/get", (req,res) =>{
    let today = new Date();
    res.json({
        message: `Get method
        `
    })

})
app.post("/post", (req,res) =>{
    let today = new Date();
    res.json({
        message: `
        POST method
        `
    })

})
app.put("/", (req,res) =>{
    let today = new Date();
    res.json({
        message: `PUT method`
    })

})
app.delete("/", (req,res) =>{
    let today = new Date();
    res.json({
        message: `
        delete method
        `
    })

})
app.listen(port, () => {
    console.log("App listening on port: " , port )
})